# Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear borads y manejo de boards](#Crear-borads-y-manejo-de-boards)


# Información de como acceder a git lab
- Se abre un navegador
- Ingresamos a https://gitlab.com/
- Iniciamos Sesion
- Si no tiene una cuenta en GitLab, se crea una cuenta



#  Crear Repositorios

Abrimos un nuevo proyecto o creamos un proyecto en blanco

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/rama1/imagenes/repoa.PNG"
width ="">

luego configuramos el repositorio

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/rama1/imagenes/repob.PNG"
width ="">

y finalmente creamos el repositorio

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/rama1/imagenes/repoc.PNG"
width ="">

# Crear Grupos

Vamos a la opcion de crear grupos

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/rama1/imagenes/grupo.PNG"
width ="">

Configuramos el grupo y agragamos miembros

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/rama1/imagenes/grupo2.PNG"
width ="">

Finalmente creamos el grupo

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/rama1/imagenes/grupo3.PNG"
width ="">

## Crear subgrupos

- 1. Creando un subgrupo
Para crear un subgrupo, debe ser propietario o mantenedor del grupo, según la configuración del grupo.

- En el panel del grupo, haga clic en el botón Nuevo subgrupo .

- 2. Crea un nuevo grupo como lo harías normalmente. Observe que el espacio de nombres del grupo principal inmediato se fija en Ruta del grupo . El nivel de visibilidad puede diferir del grupo principal inmediato.

- 3. Haga clic en el botón Crear grupo para ser redirigido a la página del panel del nuevo grupo.

## Crear issues

- Crear un ISSUE
Luego de esto, nos vamos al gitlab en el sidebar lateral izquierdo seleccionamos la opción Issue -> List y presionamos New Issue. Titulo: Un titulo claro y conciso. Descripción: Tiene que ser una descripción muy clara de que es lo que se tiene que hacer.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/issues.PNG"
width ="">

## Crear labels

Podemos hacerlo generando una etiqueta o TAG directamente desde gitlab.
...


- El procedimiento es:
- Acceder al repositorio.
- Acceder a Repositorio -> Tags o Etiquetas. 
- Crear una nueva Etiqueta con los datos completos que nos interesen.

## Roles que cumplen


<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/roles.PNG"
width ="">

## Agregar miembros

GitLab - Agregar usuarios
- Inicie sesion en su cuenta de GitLab y navegue hasta su proyecto en Proyectos .

 - Luego haga clic en la opcion Miembros debajo de la pestana Configuracion -

- Esto abrira la pantalla de abajo para agregar el miembro a su proyecto -

## Crear boards y manejo de boards

- Haga clic en el menú desplegable con el nombre del tablero actual en la esquina superior izquierda de la página Tableros de temas.
- Haz clic en Crear tablero nuevo .
- Ingrese el nombre de la nueva placa y seleccione su alcance: hito, etiquetas, cesionario o peso.


<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/board.PNG"
width ="">
