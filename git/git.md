# Menú
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

# ¿Que es el Git?

Es un software de control de versiones diseñado por Linus Torvalds, pensando en 
la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de 
aplicaciones cuando estas tienen un gran número de archivos de código fuente.

# .- Comandos de git en consola
## git init
git init es un comando que se utiliza una sola vez durante la configuración inicial de un repositorio nuevo. Al ejecutar este comando, se creará un nuevo subdirectorio . git en tu directorio de trabajo actual. También se creará una nueva rama maestra.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/GIT.PNG"
width ="">

## git status
git status muestra el estado del directorio de trabajo y del área del entorno de ensayo. Permite ver los cambios que se han preparado, los que no y los archivos en los que Git no va a realizar el seguimiento.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/comv2.PNG"
width ="">

## git clone
Si deseas obtener una copia de un repositorio Git existente — por ejemplo, un proyecto en el que te gustaría contribuir — el comando que necesitas es git clone

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone.PNG"
width ="">

## git add .
Añadir archivos al área de ensayo, git add
Esto permite construir series de instantáneas de los archivos creados o modificados en el proyecto antes de guardarlos en el historial del proyecto.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20add.PNG"
width ="">

## git touch 
El comando touch de Linux se usa principalmente para crear archivos vacíos y cambiar marcas de tiempo de archivos o carpetas

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/touch.PNG"
width ="">

## git commit 
El comando git commit captura una instantánea de los cambios preparados en ese momento del proyecto. Las instantáneas confirmadas pueden considerarse como versiones "seguras" de un proyecto: Git no las cambiará nunca a no ser que se lo pidas expresamente.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/guardar.PNG"
width ="">

## git push
El comando git push se usa para cargar contenido del repositorio local a un repositorio remoto. El envío es la forma de transferir commits desde tu repositorio local a un repositorio remoto.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/push.PNG"
width ="">

# Clientes git
Un cliente GIT o software de control de versiones se usa mayormente para gestionar código fuente. Se diseñó para el mantenimiento de las versione de las aplicaciones cuando tienen un código fuente que contiene muchos archivos.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitclient.PNG"
width ="">

# Clonación de proyecto por consola y por cliente
Si deseas obtener una copia de un repositorio Git existente — por ejemplo, un proyecto en el que te gustaría contribuir — el comando que necesitas es git clone

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone.PNG"
width ="">

Fíjate que estamos trabajando en local, como anteriormente lo hicimos la única diferencia es que se trata de un repositorio clonado, aqui pondremos file y luego clone repo
<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/kraken.PNG"
width ="">

Luego pegamos la url de la repo que queremos clonar
<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone2.PNG"
width ="">


# Commits por consola y por cliente kraken o smart

Creamos commits para poder guardar por consola cada uno de los cambios efectuados.


<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/guardar.PNG"
width ="">

En cada commit que vayas efectuando dentro del kraken deberemos seguir estos pasos:


<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/coomit2.PNG"
width ="">

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit%204.PNG"
width ="">

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/REPO1.PNG"
width ="">


 Aqui estarian agregados a cada commit

# Ramas de kraken

Nos Dirigimos hacia el git kraken y seleccionamos branch para crear la rama.

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/branch1.PNG"
width ="">

Y se nos crea la Rama en el git kraken

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/branch2.PNG"
width ="">

# Merge

La rama 1 se ubicamos en el master hasta que nos salga el marge into de la siguiente manera

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/marge2.PNG"
width ="">

Y luego se nos crea una integracion  en el git kraken

<img src="https://gitlab.com/gam.pillajo/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/marge1.PNG"
width ="">
